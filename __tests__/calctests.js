const Calc = require('../Calc');
const inputs =
    [
        [2, 41, "+", 43],
        [1, 3, "-", -2],
        [6, 2, "/", 3],
        [100, 3, "*", 300],
        [69, 3, ">", true],
        [69, 69, ">=", true],
        [69, 69, "=", true],
        [1, 2, "<=", true],
        [2, 3, "<", true]
    ];


describe('Calculator', () => {

test.each(inputs)('Testing calculations',(a,b,str,answ) =>{
    expect(Calc.calculate(a,b,str)).toBe(answ);
})
/* LEGACT CODE
    for (var i = 0; i < inputs.length; i++) {
        let a = inputs[i][0];
        let b = inputs[i][1];
        let c = inputs[i][2];
        let d = inputs[i][3];

        
        if (inputs[i][4] !== false)
            test(`is ${a} ${c} ${b} ${d}`, () => {
                expect(Calc.calculate(a, b, c)).toBe(d);
            });Ü
        else test(`isn't ${a} ${c} ${b} ${d}`, () => {
            expect(Calc.calculate(a, b, c)).toBe(!d);;

        });
    }
         for (var i = 0; i < inputs.length; i++) {
        let a = inputs[i][0];
        let b = inputs[i][1];
        let c = inputs[i][2];
        let d = inputs[i][3];

        resp = Calc.calculate(a, b, c);
        //sees if its supposed to be false
        if (inputs[i][4] !== false)
            test(`is ${a} ${c} ${b} ${d}`, () => {
                expect(resp).toBe(d);
            });
        else
            test(`isn't ${a} ${c} ${b} ${d}`, () => {
                expect(resp).toBe(false);;

            });
    }*/
    
});