const handler = require('../handler.js');

describe('handler.js:', () => {

    test('does the handler successfully respond', () => {
        handler.hello(new Event('FakeEvent')).then((response) => {
            expect(response.statusCode).toBe(200);
        })
    })
});