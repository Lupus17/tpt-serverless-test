exports.calculate = function calculate(inta, intb, string) {
  if (string === '+') {
    return inta + intb;
  }
  if (string === '-') {
    return inta - intb;
  }
  if (string === '*') {
    return inta * intb;
  }
  if (string === '/') {
    return inta / intb;
  }
  if (string === '>') {
    return inta > intb;
  }
  if (string === '>=') {
    return inta >= intb;
  }
  if (string === '<') {
    return inta < intb;
  }
  if (string === '<=') {
    return inta <= intb;
  }
  if (string === '=') {
    return inta === intb;
  }
  return null;
}