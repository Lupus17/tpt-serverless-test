const Calc = require('./Calc');

module.exports.hello = async (event) => {
  let { queryStringParameters } = event;
  if (!queryStringParameters) {
    queryStringParameters = {};
  }

  const a = parseFloat(queryStringParameters.a, 10) || 0;
  const b = parseFloat(queryStringParameters.b, 10) || 0;
  const { type } = queryStringParameters;
  const c = Calc.calculate(a, b, type);
  const resp = (c !== undefined ? `<h1>your answer to: ${a} ${type} ${b} is ${c}</h1>` : '');


  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html',
    },
    body:
    `
      <head>
        <meta charset="UTF-8">
        <title>Serverless calculator</title>
        
        <style>
        .card {
          width:500px;
          box-shadow:
          2px 2px 6px 2px rgba(134,134,134,0.82);
          background:LightGrey;
          border-radius:5px;
          
        }
        </style>
      </head>
      <body>
    <div class="card""><h1>The calculator</h1>
    <form method="get">
    <input type="text" placeholder="first value" name="a" value="${a || ''}">
    <select class="inputControls"name="type">
                    <option value="+">+</option/>
                    <option value="-">-</option/>
                    <option value="/">/</option/>
                    <option value="*">*</option/>
                    <option value="=">=</option/>
                    <option value="<"><</option/>
                    <option value=">">></option/>
                    <option value=">=">>=</option/>
                    <option value="<="><=</option/>
                </select>

    <input type="text" placeholder="second value" name="b" value="${b || ''}">
    <button type="submit">SUBMIT</button>
    <br>
    <br>
    </form>${resp}</div>
    <div class="card"><h1>Welcome to the homework of Sten Unt</h1>
    <h2>The links for the project:</h2>
    <h3>gitlab: <a href="https://gitlab.com/Lupus17/tpt-serverless-test">https://gitlab.com/Lupus17/tpt-serverless-test</a></h3>
    <h3>The forked project: <a href=https://gitlab.com/eritikass/tpt-serverless-test">https://gitlab.com/eritikass/tpt-serverless-test</a></h3>
    <h3>Serverless link:  <a href="https://lin2z7nck1.execute-api.us-east-1.amazonaws.com/dev/hello">https://lin2z7nck1.execute-api.us-east-1.amazonaws.com/dev/hello</a></h3>

    </div>
    </body>
    `,


  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
